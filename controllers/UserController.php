<?php

require_once(ROOT . '/lib/TIniFileEx.php');

class UserController {

    public function actionIndex()
    {
        //Дополнительные задания 1
        //Разработать php-скрипт, отображающий сам себя без использования функций чтения файлов.
        //в мануале php функция highlight_file относиться в раздел "Разные Функции" http://php.net/manual/ru/function.highlight-file.php
        //функции чтения фалов http://php.net/manual/ru/ref.filesystem.php
        //highlight_file(__FILE__);

        //если пользователь авторизован
        $user = User::getAuthorization();
        if (isset($user)) {
            header("Location: show");
            return true;
        }

        //вход
        if (isset($_POST["submit1"])) {
            $email_input    = $_POST['email'];
            $password_input = $_POST['passGo'];

            $time_aut = 300;//время сколько ждать пользователю
            $log_count = User::getLogCount();

            if ($log_count >= 3)//если больше или равно три неверных входа
            {
                $time = User::getTime();
                $timedifference = time() - $time;

                if ($timedifference >= $time_aut) {//если время 5мин прошло обнуляем счетчики
                    User::setTime('0');
                    User::setLogCount('0');
                    //логинемся
                    User::checkUser($email_input, $password_input);
                }
                else {//если 5 мин не прошло показать сколько осталось
                    $time_tmp = $time_aut - $timedifference;
                    echo "<br><p>Попробуйте еще раз через {$time_tmp} секунд</p>";
                }
            }
            else {
                //логинемся
                User::checkUser($email_input, $password_input);
            }
        }
        require_once(ROOT . '/views/site/login.php');
        return true;
    }

    public function actionShow()
    {
        //если пользователь не авторизован
        $user = User::getAuthorization();
        if (!isset($user)){
            header("Location: home");
            return true;
        }
        //выход
        if (isset($_POST["submit2"])) {
            $user = User::getAuthorization();
            unset($user);
            session_destroy();
            header("Location: home");
            return true;
        }
        require_once(ROOT . '/views/site/UsersShow.php');
        return true;
    }
}
