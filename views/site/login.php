<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>users</title>

    <link href="/template/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    <link href="/template/css/cover.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">

            <script src="/db/number-text.js">

            </script>
            <script>
                number_to_text(199);
            </script>

            <form class="form-signin" role="form" method="post" action="home">
                <h2 class="form-signin-heading">Вход</h2>
                <input name="email" type="email" class="form-control form-group" placeholder="Email" required autofocus>
                <input name="passGo" type="password" class="form-control" placeholder="Password" required>
                <br>
                <button name="submit1" class="btn btn-lg btn-primary btn-block form-group" type="submit">Войти</button>
            </form>
        </div>
    </div>
</body>
</html>

