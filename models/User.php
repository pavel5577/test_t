<?php

require_once(ROOT . '/lib/TIniFileEx.php');

class User {

    public static function getFileIni(){//
        return new TIniFileEx('config/users.ini');
    }
    public static function getTime(){//
        return self::getFileIni()->read('time', 'time');
    }
    public static function getLogCount(){//
        return self::getFileIni()->read('time', 'log_count');
    }
    public static function getPassword(){//
        return self::getFileIni()->read('user', 'user_pass');
    }
    public static function getEmail(){//
        return self::getFileIni()->read('user', 'user_mail');
    }
    public static function getName(){//
        return self::getFileIni()->read('user', 'user_name');
    }

    //set
    public static function setTime($time){
        $ini = self::getFileIni();
        $ini->write('time', 'time', "{$time}");
        $ini->updateFile();
    }
    public static function setLogCount($log_count){
        $ini = self::getFileIni();
        $ini->write('time', 'log_count', "{$log_count}");
        $ini->updateFile();
    }

    //проверка пароля и почты
    public static function checkText($password1, $password2){
        if(!strcmp($password1, $password2)){ return true; }
        return false;
    }

    //создать сессию
    public static function authorization($name){
        $_SESSION['user'] = $name;
    }

    //получить сессию
    public static function getAuthorization(){
        return $_SESSION['user'];
    }

    //логинемся
    public static function checkUser($email, $password)
    {
        //получаем данные с формы
        $email_input = $_POST['email'];
        $password_input = $_POST['passGo'];
        //получаем данные с файла
        $email_ini = User::getEmail();
        $password_ini = User::getPassword();
        //если данные не верны
        $errors = false;

        //проверка полей
        if (!User::checkText($email_input, $email_ini)) {
            $errors['email'] = "email не найден";
        }
        if (!User::checkText($password_input, $password_ini)) {
            $errors['pass'] = "Такой email уже используктся";
        }
        //если нет ошибок
        if ($errors == FALSE) {
            User::setTime('0');
            User::setLogCount('0');

            //создать сессию
            $name_init = User::getName();
            User::authorization($name_init);
            header("Location: show");
            return true;

        } else {//+1 неверная попытка записать в файл

            $log_count = User::getLogCount();
            $log_count++;
            User::setLogCount($log_count);

            if ($log_count >= 3)//если больше или равно три неверных входа
            {
                User::setTime(time());
                echo "<br><p>Попробуйте еще раз через 300 секунд</p>";
            } else {
                echo "<br><p>Неверные данные</p>";
            }

        }

    }
}
