/*a.*/

SELECT DISTINCT c.id, c.name FROM categories c
LEFT JOIN product_and_category pac ON pac.id_category = c.id
WHERE pac.id_product IN(3,6)

/*b.*/

SELECT p.id, p.name FROM product p
LEFT JOIN product_and_category pac ON pac.id_product = p.id
WHERE pac.id_category = 1

UNION

SELECT p.id, p.name FROM product p
LEFT JOIN product_and_category pac ON pac.id_product = p.id
WHERE pac.id_category IN(SELECT id FROM categories WHERE base_id = 1)

/*c.*/
SELECT id_category, COUNT(id_product)
FROM product_and_category
WHERE id_category IN(5,4,6)
GROUP BY id_category

/*d.*/
SELECT SUM(prod_count) count_p FROM
(
    SELECT pac.id_category, COUNT(id_product) prod_count
    FROM product_and_category pac
    WHERE pac.id_category IN(3,6)

	AND pac.id_product NOT IN
    (
    	SELECT pac2.id_product
        FROM product_and_category pac2
    	WHERE pac2.id_category IN(3,6) AND pac2.id_category != pac.id_category
    )
	GROUP BY id_category
)q1

/*e.*/

$dsn = "mysql:host=localhost; dbname= name_db";
$db = new PDO($dsn, "user_db", "password_db");
$db->exec("set names utf8");

function breadcrumb($id){
    $sql = "SELECT id, name, base_id FROM categories WHERE id = :id";
    $db->prepare($sql);
    $db->bindParam(':id', $id, PDO::PARAM_STR);
    $db->execute();
    $db->setFetchMode(PDO::FETCH_ASSOC);
    $db->fetch()

    if($db['base_id'] != 0){
        return breadcrumb($db['id'])." -> ".$db['name'];
    }
    else{
        return $db['name'];
    }
}












